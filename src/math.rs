pub fn deg2rad(v: f32) -> f32 {
    v * ((360.0 / (std::f32::consts::PI * 2.0)) as f32)
}
