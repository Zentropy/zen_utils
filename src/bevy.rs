use std::f32::consts::FRAC_PI_2;

use bevy::prelude::*;

pub trait RotateTo2D {
    fn rotate_to(&mut self, other: &Vec3);
}

impl RotateTo2D for Transform {
    fn rotate_to(&mut self, other: &Vec3) {
        let diff = *other - self.translation;
        let angle = diff.y.atan2(diff.x) - FRAC_PI_2; //sprite facing offset for upward instead of right

        self.rotation = Quat::from_axis_angle(Vec3::new(0., 0., 1.), angle);
    }
}

pub fn window_to_world(position: Vec2, window: &Window, camera_tf: &Transform) -> Vec3 {
    // Center in bevy-specific screen space
    let norm = Vec3::new(
        position.x - window.width() / 2.,
        position.y - window.height() / 2.,
        0.,
    );

    let oldpos = camera_tf.mul_vec3(norm);
    trace!("Cursor World coords: {}/{}", &oldpos.x, &oldpos.y);

    oldpos
}
