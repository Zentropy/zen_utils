#![allow(unused_imports)]

use once_cell::sync::OnceCell;

#[cfg(feature = "bevy_utils")]
pub mod bevy;
pub mod file;
pub mod math;
pub mod string;

/*
LateInit allows for delayed initialization of struct fields exactly once, making
it more semantically appropriate than simply using an Option<T> wrapper
 */
#[derive(Debug)]
pub struct LateInit<T> {
    cell: OnceCell<T>,
}

impl<T> LateInit<T> {
    pub fn init(&self, value: T) {
        assert!(self.cell.set(value).is_ok())
    }
}

impl<T> Default for LateInit<T> {
    fn default() -> Self {
        LateInit {
            cell: OnceCell::default(),
        }
    }
}

impl<T> std::ops::Deref for LateInit<T> {
    type Target = T;
    fn deref(&self) -> &T {
        self.cell.get().unwrap()
    }
}
